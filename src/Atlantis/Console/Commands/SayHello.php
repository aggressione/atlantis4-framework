<?php

namespace Atlantis\Console\Commands;
/**
 * User: gellezzz
 * Date: 19.05.21
 * Time: 21:27
 */

class SayHello extends \Illuminate\Console\Command {

    protected $signature = 'atlantis:say-hello';
    protected $description = 'dummy command';

    public function handle() {
        $this->comment('Hello!');
    }

}
