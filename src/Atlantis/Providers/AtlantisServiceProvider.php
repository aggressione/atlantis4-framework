<?php

namespace Atlantis\Providers;

use Atlaitns\Providers\RouteServiceProvider;
use Atlantis\Console\Commands\SayHello;
use Atlantis\View\Components\GuestLayout;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AtlantisServiceProvider extends ServiceProvider {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SayHello::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {

        /**
         * register artisan commands
         */
        $this->commands($this->commands);
        require __DIR__ . '/../../routes/console.php';

        /**
         * register new or merge config files
         */
        $this->mergeConfigFrom(__DIR__ . '/../../config/auth.php', 'auth');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Router $router) {

        /**
         * bind atlantis kernel
         */
        $this->app->singleton(
            \Illuminate\Contracts\Http\Kernel::class,
            \Atlantis\Http\Kernel::class
        );

        /**
         * load views
         */
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'admin');
        Blade::componentNamespace('Atlantis\\View\\Components', 'admin');

        /**
         * load migration folder
         */
        $this->loadMigrationsFrom(__DIR__. '/../../database/migrations');
    }
}
